package cn.imfc.member.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/14
 * \* Time: 下午6:06
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("memberAction")
public class MemberController {

    @Value("${server.port}")
    private String serverPort;
    @RequestMapping("getMemberAll")
    public List<String> getMemberAll(){
        List<String> listUser=new ArrayList<String>();
        listUser.add("张小龙");
        listUser.add("龙虾尾");
        listUser.add("尾巴");
        listUser.add("serverPort:"+serverPort);
        return listUser;
    }

    @RequestMapping("getMemberServiceApi")
    public String getMemberServiceApi(){
        return "这是一个会员服务";
    }
}