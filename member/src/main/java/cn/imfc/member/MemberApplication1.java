package cn.imfc.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Eureka客户端提供服务
 */
@SpringBootApplication
@EnableEurekaClient
public class MemberApplication1 {

	public static void main(String[] args) {
		SpringApplication.run(MemberApplication1.class, args);
	}
}
