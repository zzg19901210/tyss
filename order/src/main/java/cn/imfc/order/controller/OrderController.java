package cn.imfc.order.controller;

import cn.imfc.order.service.OrderMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/14
 * \* Time: 下午6:20
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("orderAction")
public class OrderController {

    @Autowired
    private OrderMemberService orderMemberService;

    @RequestMapping("getOrderUserAll")
    public List<String> getOrderUserAll(){
        return  orderMemberService.getOrderUserAll();
    }

    @RequestMapping("getOrderServiceApi")
    public String getOrderServiceApi(){
        return "这是一个订单服务";
    }
}