package cn.imfc.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/14
 * \* Time: 下午6:17
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Service
public class OrderMemberService {
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 使用rest方法调用哪个
     * @return
     */
    public List<String> getOrderUserAll(){
        return restTemplate.getForObject("http://service-member/memberAction/getMemberAll",List.class);

    }
}