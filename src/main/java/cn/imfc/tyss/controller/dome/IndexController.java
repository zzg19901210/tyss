package cn.imfc.tyss.controller.dome;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/14
 * \* Time: 上午9:37
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("indexAction")
@Slf4j
public class IndexController {

    @RequestMapping("index")
    public String index(){
        log.info("index-请求");
        return "111";
    }
}