package cn.imfc.tyss.controller.dome;

import cn.imfc.tyss.test1.service.MemberServiceTest01;
import cn.imfc.tyss.test2.service.MemberServiceTest02;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/9
 * \* Time: 下午2:34
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("dataSourceAction")
public class MybatisMultilDataSourceController {
    @Autowired
    private MemberServiceTest01 memberServiceTest01;
    @Autowired
    private MemberServiceTest02 memberServiceTest02;

    @RequestMapping("insertTest1")
    public String insertTest1(String name,Integer age){
        try {
            String a= memberServiceTest01.addUser(name,age);
            return a+"条记录添加成功，数据源一";
        }catch (Exception e){
            e.printStackTrace();
            return "error";
        }

    }
    @RequestMapping("insertTest2")
    public String insertTest2(String name,Integer age){
        try {
            String  a= memberServiceTest02.addUser(name,age);
            return a+"条记录添加成功，数据源二";
        }catch (Exception e){
            e.printStackTrace();
            return "error";
        }

    }


}