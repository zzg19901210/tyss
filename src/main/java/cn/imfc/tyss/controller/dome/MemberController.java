package cn.imfc.tyss.controller.dome;

import cn.imfc.tyss.entity.User;
import cn.imfc.tyss.service.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 演示Springboot异步调用
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/9
 * \* Time: 上午10:19
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@Slf4j
@RequestMapping("memberAction")
public class MemberController {
    // @Autowired
    private MemberService memberService;

    //增加全局配置文件
    @Value("${name}")
    private String name;
    @Value("${http_url}")
    private String httpUrl;

    @RequestMapping("addMemberAddEmail")
    public String addMemberAddEmail(){
        log.info("1");
        String result=memberService.addMemberAddEmail();
        log.info("4");
        return  result;
    }

    @RequestMapping("getname")
    public String getName(){
        return name;
    }

    @RequestMapping("getHttpUrl")
    public String getHttpUrl(){
        return  httpUrl;
    }


    @RequestMapping("addUser")
    public String addUser(String name,Integer age){
       return memberService.addUser(name,age)+",lalalal";
    }
    @RequestMapping("findByName")
    public User findByName(String name){
        return  memberService.selectUser(name);
    }
}