package cn.imfc.tyss.mapper;

import cn.imfc.tyss.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/9
 * \* Time: 上午11:03
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
// @Mapper 增加Mapper注入myabtis 无需在启动类Scan
public interface UserMapper {
    @Select("SELECT * FROM USER WHERE USER NAME=#{name}")
    User findByName(@Param("name")String name);

    @Insert("INSERT INTO USER (NAME,AGE)  VALUES(#{name},#{age})")
    int insert(@Param("name")String name,@Param("age") Integer age);
}