package cn.imfc.tyss.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/9
 * \* Time: 上午9:33
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * 使用lombok插件生成get/set 方法和使用log日志
 * \
 */
@Slf4j
@Getter
@Setter
public class User {

    private String name;
    private Integer age;
    private Integer id;


    //lombok 一定要在开发工具中安装
    //参考https://blog.csdn.net/zhglance/article/details/54931430
    public static void main(String []ages){
        User user=new User();
        user.setAge(18);
        user.setName("张志刚");
        log.info(user.toString());
    }

    @Override
    public String toString() {
        return "姓名："+name+",年龄："+age;
    }
}