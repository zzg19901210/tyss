package cn.imfc.tyss.test2.service;

import cn.imfc.tyss.entity.User;
import cn.imfc.tyss.test2.mapper.UserMapperTest02;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/9
 * \* Time: 上午10:20
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * 增加异步参数，Async
 * \
 */
@Service
@Slf4j
@Async
public class MemberServiceTest02 {
    @Autowired
    private UserMapperTest02 userMapperTest02;

    @Async //相当于这个方法重新开辟了单独的线程进行执行
    public String addMemberAddEmail(){
        String result="";
        log.info("2");
        try {

            Thread.sleep(500);
            log.info("3");
        }catch (Exception e){

        }
        return result;
    }
    //

    @Transactional(transactionManager = "test2TransactionManager")
    public String addUser(String name,Integer age){
        int insertResult=userMapperTest02.insert(name,age);
        int i=1/age;
        log.info("#######insertResult"+insertResult);
        return insertResult+":";
    }

    public User selectUser(String name){
        return  userMapperTest02.findByName(name);

    }
}