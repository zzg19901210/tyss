package cn.imfc.tyss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication //启动并扫描当前目录和子目录的所有注解类 启动时间较慢
@EnableAsync//启动异步调用
// @ComponentScan("cn.imfc.tyss.controller","cn.imfc.tyss.service")扫描
// @MapperScan("cn.imfc.tyss.mapper")//多个注解以逗号分隔
//-XX:+PrintGCDetails -Xmx1024 -Xms1024M
//-XX:+PrintGCDetails -Xmx32M -Xms1M
//打印详细GC日志 最大堆内存为32m,初始化为1M
//java -server -Xmx32M -Xms32M -jar
//开启读取配置
public class TyssApplication {

	public static void main(String[] args) {
		SpringApplication.run(TyssApplication.class, args);
	}
}
