package cn.imfc.tyss.error;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@ControllerAdvice(basePackages = "cn.imfc.tyss.controller")
public class GlobalExceptionHandler {

    @ExceptionHandler(RuntimeException.class  )
    @ResponseBody
    public Map<String,Object> errorResult(){

        //实际开发中会把错误日志记录到日志表中。
        Map<String ,Object> errorResult=new HashMap<String, Object>();

        errorResult.put("errorCode","500");
        errorResult.put("errorMsg","系统错误");
        return  errorResult;

    }


}
