package cn.imfc.tyss.test1.service;

import cn.imfc.tyss.entity.User;
import cn.imfc.tyss.test1.mapper.UserMapperTest01;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/9
 * \* Time: 上午10:20
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * 增加异步参数，Async
 * \
 */
@Slf4j
@Async
@Service
public class MemberServiceTest01 {
    @Autowired
    private UserMapperTest01 userMapperTest01;

    @Async //相当于这个方法重新开辟了单独的线程进行执行
    public String addMemberAddEmail(){
        String result="";
        log.info("2");
        try {

            Thread.sleep(500);
            log.info("3");
        }catch (Exception e){

        }
        return result;
    }
    //增加多数据源事务回滚
    @Transactional(transactionManager = "test1TransactionManager")
    public String addUser(String name,Integer age){
        int insertResult=userMapperTest01.insert(name,age);
        int i=1/age;
        log.info("#######insertResult"+insertResult);
        return insertResult+":";
    }

    public User selectUser(String name){
        return  userMapperTest01.findByName(name);

    }
}