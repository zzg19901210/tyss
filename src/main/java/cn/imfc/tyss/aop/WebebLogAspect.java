package cn.imfc.tyss.aop;

import cn.imfc.tyss.controller.dome.FTIndex;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/9
 * \* Time: 上午8:43
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Aspect
@Component
@Slf4j
public class WebebLogAspect {

    //初始化文件
    // private static final Logger logger = LoggerFactory.getLogger(WebebLogAspect.class);

    /**
     * 拦截方法配置
     */
    @Pointcut("execution(public * cn.imfc.tyss.controller.dome.*.*(..))")
    public  void webLog(){

    }

    /**
     * 使用aop前置通知
     * @param joinPoint
     * @throws Throwable
     */
    @Before("webLog()")
    public void doBeore(JoinPoint joinPoint)throws  Throwable{
        //转换为httpserver
        ServletRequestAttributes attributes= (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request=attributes.getRequest();

        //打印日志
        //处理日志存储到nosql数据库
        // logger.info("URL:"+request.getRequestURL().toString());
        // logger.info("HTTP_METHOD:"+request.getMethod());
        // logger.info("IP:"+request.getRemoteAddr());
        // Enumeration<String > enu=request.getParameterNames();
        //
        // while (enu.hasMoreElements()){
        //     String name=enu.nextElement();
        //     logger.info("name:{},value:{}",name,request.getParameter(name));
        // }打印日志
        //处理日志存储到nosql数据库
        log.info("URL:"+request.getRequestURL().toString());
        log.info("HTTP_METHOD:"+request.getMethod());
        log.info("IP:"+request.getRemoteAddr());
        Enumeration<String > enu=request.getParameterNames();

        while (enu.hasMoreElements()){
            String name=enu.nextElement();
            log.info("name:{},value:{}",name,request.getParameter(name));
        }

    }

    @AfterReturning(returning = "ret",pointcut = "webLog()")
    public void doAfterReturning(Object ret) throws Throwable{
        //处理完成
        log.info("RESPONSE："+ret);
    }

}