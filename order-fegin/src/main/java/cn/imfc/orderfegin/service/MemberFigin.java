package cn.imfc.orderfegin.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/16
 * \* Time: 下午6:57
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@FeignClient(value = "service-member")
public interface MemberFigin {
    @RequestMapping("/memberAction/getMemberAll")
    public List<String> getToOrderMemberAll();

}