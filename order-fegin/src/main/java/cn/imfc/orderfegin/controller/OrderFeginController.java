package cn.imfc.orderfegin.controller;

import cn.imfc.orderfegin.service.MemberFigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/16
 * \* Time: 下午6:59
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
public class OrderFeginController {

    @Autowired
    private MemberFigin memberFigin;
    @RequestMapping("/getToOrderMemberAll")
    public List<String> getToOrderMemberAll(){
        return  memberFigin.getToOrderMemberAll();
    }

}