package cn.imfc.configclick.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zhangzhigang
 * \* Date: 2018/8/16
 * \* Time: 下午6:26
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
public class TestController {
    @Value("${username}")
    private String username;

    @RequestMapping("/getUsername")
    public String getUsername(){
        return  username;
    }
}