package cn.imfc.configclick;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigclickApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigclickApplication.class, args);
	}
}
